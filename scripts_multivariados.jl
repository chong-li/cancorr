using Gadfly
using Distributions
using Cairo
using Atom
Gadfly.push_theme(:default)
#Gadfly.push_theme(:dark)

function homocedastica(p::Int64, ρ)
    return (1-ρ)*eye(Float64,p)+ ρ*ones(Float64,ρ)
end
homocedastica(μ, ρ) = homocedastica(length(μ), ρ)

#Retorna 2-upla (Λ,V) tal que V^(-1)ΛV = Σ
function homocedastica_diagonalizada(p::Int64, ρ)
    λ = (1-ρ)*ones(Float64,p)
    λ[1] = 1-ρ + p*ρ
    Λ = Diagonal(λ)*eye(Float64,p)
    # define matriz de autovetores segundo solução do exercício 2b
    V = (1/sqrt(2))*eye(Float64,p)
    V[1,:] = (-1/sqrt(2))*ones(Float64,p)
    V[:,1] = (1/sqrt(p))*ones(Float64,p)
    return Λ,V
end
homocedastica_diagonalizada(μ, ρ) = homocedastica_diagonalizada(length(μ), ρ)

function destransforma(z, μ, Σs)
    return Σs*z .+ μ
end

function aas(n::Int64, μ, Σs)
    p = length(μ)
    n==1 ? z = randn(p): z = randn(p,n)
    return destransforma(z,μ,Σs)
end
aas(μ, Σs) = aas(1, μ, Σs)
# É possível chamar aas(μ, (Λ, V)...)
aas(n::Int64, μ, Λ, V) = aas(n, μ,(V^(-1))*(sqrt.(Λ))*V)
aas(μ, Λ, V) = aas(1, μ, Λ, V)

function χQuadradoLenta(degfree::Int64)
    χ = randn(degfree).^2
    return sum(χ)
end

function randχsq(degfree::Int64, n::Int64)
    return map(x->χQuadradoLenta(degfree), Array{Float64}(n))
end

function matrizS(Y)
    n,p = size(Y)
    H = eye(Float64,n)- (1/n)*ones(Float64,n,n)
    return (1/n)*(Y')*H*Y
end

function CPA(Y)
    return ordenaeigs(eig(matrizS(Y))...)
end

function ordenaeigs(λ,V)
    p = sortperm(λ,rev=true)
    return λ[p], V[:,p]
end

function factorCPA(Y, m::Int64=0)
    X = Y .- mean(Y,1)
    λ,V = CPA(X)
    Φ = V[:,1:end-m]*(Diagonal(sqrt.(λ[1:end-m])))
    Ψ = Diagonal(matrizS(X)-Φ*(Φ'))
    return Φ,Ψ
end

function dados3()
    D = [7 6 21 15;
        6 6 14 11;
        9 12 17 12;
        6 8 12 10;
        10 13 16 12;
        8 7 14 9;
        7 6 14 8;
        6 9 10 5;
        5 6 19 16;
        5 4 16 10;
        8 6 17 10;
        7 6 11 10;
        7 10 9 12;
        7 6 9 11;
        9 7 18 12;
        8 10 15 11;
        5 4 12 13;
        5 8 11 10;
        7 7 10 11;
        4 6 9 11;
        8 9 20 17;
        9 12 18 13;
        8 10 15 12;
        9 11 19 12]
        return D
end
dados3(lin::UnitRange,col::UnitRange) = dados3()[lin,col]
# m de modalidade (A=1,B=2,C=3).
dados3(m) = dados3( (-7:0)+(8*m), 1:4)
# lesao: sim = 1, não = 2
dados3(m,lesao) = dados3(m)[(-3:0)+(4*lesao),:]

function class3(w)
    W = sum(x->size(x)[1]*cov(x,1,corrected=false),w)
    B = map(x->mean(x,1),w)
    B = vcat(B...)
    Bc = 4*size(B)[1]*cov(B,1,corrected=false)
    λ,V = ordenaeigs(eig(W\Bc)...)
    #= n será 0 se todos os autovalores forem muito pequenos
        nese caso é certamente necessário ajustar escalas=#
    n = findlast(k->ispico(k) == false, λ)
    return B,V[:,1:n],λ[1:n]
    end
end

function fisher(B,V)
    return mean(B,1)*V[:,1]
end

function class3(x::RowVector,w)
    B,V,λ = class3(w)
    return mindist(x,B,V,λ)
end
class3(x::Vector,w) = class3(x',w)

function class3(x::Matrix,w)
    B,V,λ = class3(w)
    return mapslices(k->mindist(k,B,V,λ),x,2)
end

function mindist(x::RowVector,B,V,λ)
    #Λ = Diagonal(sqrt.(abs.(λ)))
    Λ = Diagonal(λ)
    #println("Λ:",size(Λ),", B:",size(B),", V:",size(V))
    d = (B .- x)*V
    d = d*Λ*d'
    return findmin(diag(d))[2]
end
mindist(x::Vector,B,V,λ) = mindist(x',B,V,λ)

function corr_can(X,Y)
    n = size(X)[1]
    one = ones(eltype(X),n)
    # Σx <- Σx^(-1/2)
    λx, Λx = CPA(X)
    Σx = Λx*(Diagonal(λx.^(-0.5)))*(Λx')
    λy, Λy = CPA(Y)
    Σy = Λy*(Diagonal(λy.^(-0.5)))*(Λy')
    Σ = (1/n)*((X')*Y - (1/n)*((X')*one)*((one')*Y))
    M = Σx*Σ*Σy
    λm, U = ordenaeigs(eig(M)...)
    return sqrt.(λm), U'*Σx, U*Σy
end

function tresC()
    Mas = dados3(1,1)
    Man = dados3(1,2)
    Mbs = dados3(2,1)
    Mbn = dados3(2,2)
    Mcs = dados3(3,1)
    Mcn = dados3(3,2)
    w = [Mas, Man, Mbs, Mbn, Mcs, Mcn]
    return w
end

function tresD(m)
    return [dados3(m,1), dados3(m,2)]
end

function ispico(x)
    if abs(x) < 10.0^(-12)
         return true
    else return false
    end
end
ispico(x::Array) = all(ispico.(x))
#plot(x=rand(Normal(), 100), y=Normal(), Stat.qq, Geom.point);

#testgraph = plot(x=rand(Normal(), 100), y=rand(Normal(),100), Stat.qq, Geom.point,intercept=[0], slope=[1], Geom.abline(color="blue"));

#draw(PDF("testgraph.pdf", 8inch, 8inch), testgraph)
